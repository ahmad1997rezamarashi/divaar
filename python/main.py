from modules import report
from modules.divaar_scraping import open_web_page_in_bs, PostPage, CategoryPage
from settings import HOME_URL, FULL_URL, RECENT_POST_NUM


def get_category_links():
    first_page = open_web_page_in_bs(HOME_URL)
    category_list_attr = first_page.find(
        'ul', class_="filter-category-list")
    cat_links = []

    for cat_list_item in category_list_attr.children:
        link = FULL_URL.format(cat_list_item.a['href'])
        cat_links.append(link)

    return cat_links


def create_post_obj(post_attr, post_tags):
    post_link = FULL_URL.format(post_attr['href'])

    post_page = PostPage(post_link)
    post_title = post_page.get_title()
    post_fields_object = post_page.get_fields_array()

    return {"title": post_title, "tags": post_tags, "fields": post_fields_object}


if __name__ == '__main__':

    category_links = get_category_links()

    for i, cat_link in enumerate(category_links):

        category_page = CategoryPage(cat_link)

        category_report_obj = {}
        recent_post_list = category_page.get_recent_post_list(RECENT_POST_NUM)
        recent_post_tag_list = category_page.get_recent_post_tags_list(RECENT_POST_NUM)

        for index in range(RECENT_POST_NUM):
            post_attr_item = recent_post_list[index]
            post_tags_item = recent_post_tag_list[index]
            category_report_obj[index] = create_post_obj(post_attr_item, post_tags_item)

        report.report_json(category_report_obj, "report-{}.json".format(i))

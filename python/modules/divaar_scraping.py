import requests
from bs4 import BeautifulSoup

from settings import FULL_URL


def open_web_page_in_bs(link):
    web_source = requests.get(link)
    soup = BeautifulSoup(web_source.text, 'lxml')
    return soup


class PostPage:

    def __init__(self, link):
        page_attr = open_web_page_in_bs(link)
        self.info_attr = page_attr.find('div', class_="post-page__info")

    def __get_filed_list(self):
        fields_list = self.info_attr.find_all("div", class_="post-fields-item")
        return list(fields_list)

    def get_title(self):
        title_attr = self.info_attr.find('h1', class_="post-header__title")
        title = title_attr.text
        return title

    def get_fields_array(self):
        fields_array = []
        fields_list = self.__get_filed_list()
        for filed in fields_list:
            key = list(filed.children)[0].text
            value = list(filed.children)[1].text
            fields_array.append({"key": key, "value": value})
        return fields_array


class CategoryPage:
    @staticmethod
    def get_tags_list( post_attr):
        tag_attr_list = list(post_attr.find_all(
            'span', class_="p-lr-xsmall post-card__info-urgent"))
        post_tags = [tag_item.text for tag_item in tag_attr_list]
        return post_tags

    def __init__(self, link):
        category_page = open_web_page_in_bs(link)
        self.posts_attr = category_page.find(
            'div', class_="browse-post-list")

    def get_recent_post_list(self, post_num=10):
        return list(self.posts_attr.children)[:post_num]

    def get_recent_post_tags_list(self, post_num=10):
        recent_post = self.get_recent_post_list(post_num)
        return [CategoryPage.get_tags_list(post_attr) for post_attr in recent_post]

import json
import os

from settings import REPORT_DIR_PATH, REPORT_JSON_INDENT


def create_folder(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def report_json(report, file_name):
    create_folder(REPORT_DIR_PATH)
    with open(REPORT_DIR_PATH + file_name, "w", encoding="utf8") as report_file:
        data = json.dumps(report, indent=REPORT_JSON_INDENT, ensure_ascii=False)
        report_file.write(data)

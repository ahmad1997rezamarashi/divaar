BASE_URL = "https://divar.ir"
HOME_URL = "https://divar.ir/s/tehran"
REPORT_DIR_PATH = "reports/"
FULL_URL = "https://divar.ir{}"
REPORT_JSON_INDENT = 2
RECENT_POST_NUM = 10

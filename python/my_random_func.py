import random


def generate_random_number(range_num, history_size):
    history = []

    def get_random():
        random_num = random.randrange(1, range_num)
        while random_num in history:
            random_num = random.randrange(1, range_num)

        history.append(random_num)
        if len(history) > history_size:
            history.pop(0)

        print(history)

        return random_num

    return get_random


my_random_generator = generate_random_number(1000000, 10)

for i in range(110):
    print(my_random_generator())

درباره‌ی کلاینت و سرور سرچ کن و یاد بگیر دقیقا چی هستن.
What is a Client?

A client is a computer hardware device or software that accesses a service made available by a server. The server is often (but not always) located on a separate physical computer.


What is a Server?

A server is a physical computer dedicated to run services to serve the needs of other computers. Depending on the service that is running, it could be a file server, database server, home media server, print server, or web server.


پورت چیه؟
a port (noun) is a "logical connection place" and specifically, using the Internet's protocol, TCP/IP



Port numbers

    The Well Known Ports are those from 0 through 1023.
    The Registered Ports are those from 1024 through 49151.
    The Dynamic and Private Ports are those from 49152 through 65535.



network commands:
** ifconfig:
ifconfig -a
 interface..
hostname
hostid
tcpdump -c 10 -A -i sds
tcpdump -c 10 -XX -l -i sds
tcpdump -c 10 -XX -l port 23
tcpdump -i eth0 


**netstat

netstat -a
netstat -au : To list all udp ports.
netstat -l : To list only the listening ports.
netstat -lt : To list only the listening tcp ports.
netstat -lu : To list only the listening udp ports.
netstat -pt : To display the PID and program names.
netstat -ap | grep ssh : To get the port
on which a program is running.
netstat -an | grep ':80' : To get the process
which is using the given port.
netstat -i : To get the list of network interfaces.

netstat -nr
netstat -tulpn | grep -i "listen"
-t : All TCP ports
-u : All UDP ports
-l : Display listening server sockets
-p : Show the PID and name of the program to which each socket belongs
-n : Don’t resolve names

***nc :

nc -l 2399
nc localhost 2399
-u
><



sudo lsof -i -P -n | grep LISTEN
-i : Look for listing ports
-P : Inhibits the conversion of port numbers to port names for network files. Inhibiting the conversion may make lsof run a little faster. It is also useful when port name lookup is not working properly.
 -n : Do not use DNS name
ls


